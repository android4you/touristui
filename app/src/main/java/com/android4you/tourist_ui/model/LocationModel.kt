package com.android4you.tourist_ui.model

data class LocationModel(
    val placeName: String,
    val latitude: Long,
    val longitude: Long,
    val type: String,
    val timing: Long,
    val distance: Long
)
