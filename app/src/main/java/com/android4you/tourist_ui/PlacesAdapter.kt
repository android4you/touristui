package com.android4you.tourist_ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.android4you.tourist_ui.model.LocationModel

class PlacesAdapter (private var placeList: List<LocationModel>) :
    RecyclerView.Adapter<PlacesAdapter.MyViewHolder>() {
     class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var locationTV: TextView = view.findViewById(R.id.location)
        var timingTV: TextView = view.findViewById(R.id.timing)
        var distanceTV: TextView = view.findViewById(R.id.distance)
    }
    @NonNull
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_place_layout, parent, false)
        return MyViewHolder(itemView)
    }
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val movie = placeList[position]
        holder.locationTV.text = movie.placeName
       // holder.timingTV.text = movie.timing.toString()
        //holder.distanceTV.text = movie.distance.toString()
    }
    override fun getItemCount(): Int {
        return placeList.size
    }
}