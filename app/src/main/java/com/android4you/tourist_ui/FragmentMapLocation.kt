package com.android4you.tourist_ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.android4you.tourist_ui.model.MyItem
import com.android4you.tourist_ui.model.MyItemReader
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.maps.android.clustering.ClusterManager
import org.json.JSONException

class FragmentMapLocation: Fragment(R.layout.fragment_map_location), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap



    private var mClusterManager: ClusterManager<MyItem>? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

    }


    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

//        // Add a marker in Sydney and move the camera
//        val sydney = LatLng(-34.0, 151.0)
//        mMap.addMarker(MarkerOptions().position(sydney).title("Marker in Sydney"))
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney))
        startDemo(false)
    }


    protected fun startDemo(isRestore: Boolean) {

        if (!isRestore) {
            mMap.moveCamera(
                CameraUpdateFactory.newLatLngZoom(
                    LatLng(51.503186, -0.126446),
                    12f
                )
            )
        }

        mClusterManager = ClusterManager(requireContext(), mMap)
        mMap.setOnCameraIdleListener(mClusterManager)

        // Add a custom InfoWindowAdapter by setting it to the MarkerManager.Collection object from
        // ClusterManager rather than from GoogleMap.setInfoWindowAdapter
        mClusterManager!!.getMarkerCollection().setInfoWindowAdapter(object : InfoWindowAdapter {
            override fun getInfoWindow(marker: Marker): View {
                val inflater = LayoutInflater.from(requireContext())
                val view: View = inflater.inflate(R.layout.custom_info_window, null)
                val textView = view.findViewById<TextView>(R.id.textViewTitle)
                val text = if (marker.title != null) marker.title else "Cluster Item"
                textView.text = text
                return view
            }

            override fun getInfoContents(marker: Marker): View {
                return null!!
            }
        })
        mClusterManager!!.getMarkerCollection().setOnInfoWindowClickListener { marker: Marker? ->
            Toast.makeText(
                requireContext(),
                "Info window clicked.",
                Toast.LENGTH_SHORT
            ).show()
        }
        try {
            readItems()
        } catch (e: JSONException) {
            Toast.makeText(requireContext(), "Problem reading list of markers.", Toast.LENGTH_LONG).show()
        }
    }

    @Throws(JSONException::class)
    private fun readItems() {
        val inputStream = resources.openRawResource(R.raw.radar_search)
        val items: List<MyItem> = MyItemReader().read(inputStream)
        mClusterManager!!.addItems(items)
    }

}