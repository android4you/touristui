package com.android4you.tourist_ui

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.android4you.tourist_ui.model.LocationModel

class FragmentPlaces: Fragment(R.layout.fragment_places) {

    private val placeList = ArrayList<LocationModel>()
    private var placeListView: RecyclerView? = null
    private var adapter: PlacesAdapter? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        placeListView = view.findViewById(R.id.placeListView)

        for(m in 1..300){
            placeList.add(LocationModel("Thiruvanamthapuram", 10L,10L,"Heritage",10L,10L))
        }

        adapter = PlacesAdapter(placeList)
        placeListView!!.adapter = adapter


    }
}