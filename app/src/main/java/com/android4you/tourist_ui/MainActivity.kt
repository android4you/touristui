package com.android4you.tourist_ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import com.google.android.material.floatingactionbutton.FloatingActionButton

class MainActivity : AppCompatActivity() {
    private var floatingActionButton: FloatingActionButton? = null
    var i = 1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        val inflater = navHostFragment.navController.navInflater
        val graph = inflater.inflate(R.navigation.nav_graph)




        floatingActionButton = findViewById(R.id.floatingActionButton)
        floatingActionButton!!.setOnClickListener {
            if (i == 1) {
                i = 0
                graph.startDestination = R.id.fragmentPlaces
                val navController = navHostFragment.navController
                navController.setGraph(graph, intent.extras)
                floatingActionButton!!.setImageResource(R.drawable.ic_maps)

        }else {
                i = 1
                graph.startDestination = R.id.fragmentMapLocation
                val navController = navHostFragment.navController
                navController.setGraph(graph, intent.extras)
                floatingActionButton!!.setImageResource(R.drawable.ic_lists)
            }
        }

    }
}